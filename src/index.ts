import express, { Request, Response } from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import usersRoute from "./routes/users/users.js";
import { logger } from "./middleware/logger.middleware.js";
import usersModel from "./db/users.model.js";
import { setID } from "./middleware/id.middleware.js";
import { loggerError, responseWithErrorMiddleware, urlNotFoundMiddleware } from "./middleware/errors.middleware.js";
import { setUsers } from "./middleware/users.middleware.js";

const { PORT, HOST = 'localhost' } = process.env;

const USERS_DB_FILE_PATH = "./users-data.json";
const LOG_FILE_PATH = "./src/logs/users.log";
const ERROR_LOG_FILE_PATH = "./src/logs/errors.log";

const app = express();

app.use(morgan('dev'));
app.use(express.json()); // parse application/json
app.use(setID);
app.use(setUsers);
app.use(logger(LOG_FILE_PATH)); // log each request to users.log

app.use("/api/users", usersRoute);

app.get("/", (req, res) => {
    res.status(200).json("Home Page");
});

app.use(urlNotFoundMiddleware);
app.use(loggerError(ERROR_LOG_FILE_PATH));
app.use(responseWithErrorMiddleware);

app.listen(Number(PORT), HOST,  async () => {
    await usersModel.init(USERS_DB_FILE_PATH);
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});