import { NextFunction, Request, Response } from "express";
import UsersModel from "../db/users.model.js";

export const setUsers = async (req: Request, res: Response, next: NextFunction) => {
    req.users = UsersModel.users;
    next();
}