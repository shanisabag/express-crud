import { NextFunction, Request, Response} from "express"
import HttpException from "../exceptions/http.exception.js";
import fs from "fs/promises";

import UrlNotFoundException from "../exceptions/urlNotFound.exception.js"

// url not found in index.js
export const urlNotFoundMiddleware  = (req: Request, res: Response, next: NextFunction) => {
    next(new UrlNotFoundException(req.path));
}

// print

// log error
export const loggerError = (path: string) => async (err: HttpException, req: Request, res: Response, next: NextFunction) => {
    const newLog = `
    ${req.id} 
    ${err.status} ${err.stack} \r\n
    `;
    await fs.appendFile(path, newLog);
    next(err);
}

// response with error
export const responseWithErrorMiddleware  = (err: HttpException | UrlNotFoundException ,req: Request, res: Response, next: NextFunction) => {
    res.status(err.status).send(err.message);
}