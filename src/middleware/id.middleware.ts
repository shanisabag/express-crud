import { NextFunction, Request, Response } from "express";
import { uuid } from "uuidv4";

export const setID = async (req: Request, res: Response, next: NextFunction) => {
    req.id = uuid();
    next();
}