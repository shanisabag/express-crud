import express from "express";
import log from "@ajar/marker";

import { IUser } from "../../typing";
import { setUsers } from "../../middleware/users.middleware.js";
import { addUser } from "../../validations/addUser.validation.js";
import { displayUser } from "../../validations/displayUser.validation.js";
import { updateUser } from "../../validations/updateUser.validation.js";
import { deleteUser } from "../../validations/deleteUser.validation.js";

const DB_PATH = "./users-data.json";

const router = express.Router();

router.use(express.json());

// display all users
router.get('/', (req, res) => {
    res.status(200).json(req.users);
});

// display a user
router.get('/:userId', (req, res, next) => {
    try {
        const user = displayUser(req.params.userId, req.users, `/api/users${req.path}`);
        res.status(200).json(user);
    } catch (error) {
        next(error);
    }
});

// add a user
router.post('/', async (req, res, next) => {
    try {
        const newUser = await addUser((req.body as IUser), req.users, DB_PATH);
        res.status(200).send(`${(newUser as IUser).username} added successfully`);
    } catch (error) {
        next(error); 
    }
});

// update user's name 
router.patch('/:userId', async (req, res, next) => {
    try {
        const updatedUser = await updateUser(req.params.userId, req.users, `/api/users${req.path}`, (req.body as IUser), DB_PATH);
        res.status(200).send(`${(updatedUser as IUser).username} updated successfully`);
    } catch (error) {
        next(error); 
    }
});

// delete a user
router.delete('/:userId', async (req, res, next) => {
    try {
        const username = await deleteUser(req.params.userId, req.users, `/api/users${req.path}`, DB_PATH);
        res.status(200).send(`${username} deleted successfully`);
    } catch (error) {
        next(error); 
    }
});

export default router;


