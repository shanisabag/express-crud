import fs from "fs/promises";
import { IUser } from "../typing";

class UsersModel {
    users_db_path: string = "";
    users: IUser[] = [];

    async init(path: string) {
        this.users_db_path = path;

        // check if path exists, if not create one 
        // set this.users
        const stat = await fs.stat(path);
        if (stat.size > 0) {
            this.users = await this.load(path);
        } else {
            this.save(path, []);
        }
    }

    private async load(path: string) {
        const data = await fs.readFile(path, 'utf-8');
        return JSON.parse(data) as IUser[];
    }

    async save(path: string, data: IUser[]): Promise<void> {
        await fs.writeFile(path, JSON.stringify(data, null, 2));
        this.users = await this.load(path);
    }
}

const instance = new UsersModel()
export default instance;