export interface IUser {
    id: string,
    firstName: string,
    lastName: string,
    username: string
}

// declaration merging
declare global {
    namespace Express {
        interface Request {
            users: IUser[];
            id: string;
        }
    }
}

