import UrlNotFoundException from "../exceptions/urlNotFound.exception.js";
import { IUser } from "../typing";
import UsersModel from "../db/users.model.js";

export async function updateUser(userId: string, users: IUser[], reqPath: string, userData: IUser, dbPath: string): Promise<IUser | UrlNotFoundException | Error> {
    const userIndex = users.findIndex(user => user.id === userId);
    if (userIndex === -1) {
        throw new UrlNotFoundException(reqPath);
    } else {
        const updatedUser = {
            ...users[userIndex], 
            ...userData
        };
        const updatedUsers = [...users.slice(0, userIndex), updatedUser, ...users.slice(userIndex + 1)];
        await UsersModel.save(dbPath, updatedUsers);
        return updatedUser;
    }
}