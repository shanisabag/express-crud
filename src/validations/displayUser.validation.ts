import UrlNotFoundException from "../exceptions/urlNotFound.exception.js";
import { IUser } from "../typing";

export function displayUser(userId: string, users: IUser[], reqPath: string): IUser | UrlNotFoundException | Error {
    const user = users.find(user => user.id === userId);
    if (user === undefined) {
        throw new UrlNotFoundException(reqPath);
    } else {
        return user;
    }
}