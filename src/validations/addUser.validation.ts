import { uuid } from "uuidv4";
import HttpException from "../exceptions/http.exception.js";
import { IUser } from "../typing";
import UsersModel from "../db/users.model.js";

export async function addUser(userData: IUser, users: IUser[], dbPath: string): Promise<IUser | HttpException | Error> {
    const { firstName, lastName, username } = userData;
    if (!firstName || !lastName || !username) {
        throw new HttpException(422, "missing required parameters");
    } else {
        const index = users.findIndex((u) => u.username === userData.username)
        if (index !== -1) {
            throw new HttpException(409, "username already exists");
        } else {
            const user: IUser = {
                id: uuid(),
                firstName,
                lastName,
                username
            };
            const updatedUsers = [...users, user];
            await UsersModel.save(dbPath, updatedUsers);
            return user;
        }
    }
}