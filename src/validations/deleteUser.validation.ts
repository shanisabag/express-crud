import UrlNotFoundException from "../exceptions/urlNotFound.exception.js";
import UsersModel from "../db/users.model.js";
import { IUser } from "../typing";

export async function deleteUser(userId: string, users: IUser[], reqPath: string, dbPath: string): Promise<string | UrlNotFoundException | Error> {
    const userIndex = users.findIndex(user => user.id === userId);
    if (userIndex === -1) {
        throw new UrlNotFoundException(reqPath);
    } else {
        const username = users[userIndex].username;
        const updatedUsers = [...users.slice(0, userIndex), ...users.slice(userIndex + 1)];
        await UsersModel.save(dbPath, updatedUsers);
        return username;
    }
}